import redis
import time
import threading

def publisher(n, conn):
    time.sleep(1)
    for i in xrange(n):
        conn.publish('channel', i)
        time.sleep(1)

def run_pubsub(conn):
    threading.Thread(target=publisher, args=(3, conn)).start()
    pubsub = conn.pubsub()
    pubsub.subscribe(['channel'])
    count = 0
    for item in pubsub.listen():
        print item
        count += 1
        if count == 4:
            pass
            #pubsub.unsubscribe()
        # if count == 5:
        #     break

if __name__ == '__main__':
    conn = redis.Redis(host='localhost', port=6379)
    run_pubsub(conn)

