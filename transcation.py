import time
import redis
import threading

def notrans(conn):
    print conn.incr('notrans:')
    time.sleep(.1)
    conn.incr('notrans:', -1)

def trans(conn):
    p = conn.pipeline()
    p.incr('trans:')
    time.sleep(0.1)
    p.decr('trans:')
    print p.execute()[0]

if __name__ == '__main__':
    conn = redis.Redis()
    for i in xrange(3):
        threading.Thread(target=trans, args=(conn, )).start()
        threading.Thread(target=notrans, args=(conn,)).start()
    time.sleep(0.5)
